import React from 'react';
import './index.scss';
import { Modal, Button, Input, Form, message } from 'antd';
import { IRegistrationProp, IUserRegisterRequest } from 'shared/types';
import api from 'api';

export default function Registration({ isShowed, setVisible }: IRegistrationProp) {
  const onFinish = (values: any) => {
    getUserRegister({ userName: values.username, password: values.password, email: values.email });
    console.log('success', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  const getUserRegister = async (values: IUserRegisterRequest) => {
    try {
      const response = await api.getUserRegister(values);
      console.log('response', response);
    } catch (error: any) {
      message.error(error.message);
    }
  };
  return (
    <Modal className="registration" visible={isShowed} onCancel={() => setVisible(false)} footer="">
      <div className="registration__container">
        <h2 className="title">Registration</h2>
        <Form
          layout="vertical"
          className="input"
          name="input"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off">
          <Form.Item label="Email" name="email" rules={[{ required: true, message: 'Please input your email!' }]}>
            <Input size="large" className="input__margin-top-bottom" placeholder="Input email" />
          </Form.Item>
          <Form.Item
            label="Username"
            name="username"
            rules={[{ required: true, message: 'Please input your username!' }]}>
            <Input size="large" className="input__margin-top-bottom" placeholder="Input username" />
          </Form.Item>
          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
            hasFeedback>
            <Input.Password size="large" className="input__margin-top-bottom" placeholder="Input password" />
          </Form.Item>
          <Form.Item
            label="Confirm Password"
            name="confirm-password"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(__, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('The two passwords that you entered do not match!'));
                },
              }),
            ]}>
            <Input.Password size="large" className="input__margin-top-bottom" placeholder="Input password" />
          </Form.Item>
          <Button size="large" className="register--btn" htmlType="submit" type="primary" block>
            Register
          </Button>
        </Form>
        <div className="login">
          <h3>
            Already have an Account ? <span onClick={() => setVisible(false)}>Login</span>
          </h3>
        </div>
      </div>
    </Modal>
  );
}
