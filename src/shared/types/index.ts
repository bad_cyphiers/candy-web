export interface IUserRegisterRequest {
  userName: string;
  password: string;
  email: string;
}

export interface IUserRegisterResponse {
  id: number;
  startDate: null;
  modifiedDate: null;
  userName: string;
  firstName: string;
  lastName: string;
  dateOfBirth: null;
  email: string;
  password: string;
  phoneNumber: null;
  createdDate: Date;
  updatedDate: Date;
  lastLogin: Date;
  loginRetryTime: null;
  skype: null;
  facebook: null;
  note: null;
  status: boolean;
  locked: boolean;
}
export interface IUserLoginRequest {
  username: string;
  password: string;
}
export interface IUserLoginResponse {
  jwttoken: string;
}

export interface IPortal {
  username: string;
}
export interface IRegistrationProp {
  isShowed: boolean;
  setVisible: (a: boolean) => void;
}
export interface ICreateAppointmentProp {
  isShowed: boolean;
  setVisible: (a: boolean) => void;
  candidate_id: string;
}
export interface ICreateCandidateProp {
  isShowed: boolean;
  setVisible: (a: boolean) => void;
}

export interface ISearchCandidateAutocompleteProp {
  candidate_id: string;
}
