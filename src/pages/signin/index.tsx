import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Input, Button, Form, message } from 'antd';
import './index.scss';
import signInImage from '../../assets/images/sign-in-img.svg';
import Registration from '../../components/popup-registration';
import { IUserLoginRequest } from 'shared/types';
import api from 'api';

export default function SignIn() {
  const [registrationPopup, setRegistrationPopup] = useState(false);

  const navigate = useNavigate();

  const showRegistrationPopup = () => {
    setRegistrationPopup(true);
  };
  const onFinish = (values: any) => {
    getUserLogin({ username: values.username, password: values.password });
    navigate('/', { replace: true });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  const getUserLogin = async (values: IUserLoginRequest) => {
    try {
      const response = await api.getUserLogin(values);
      response.jwttoken ?? localStorage.setItem('jwttoken', response.jwttoken);
    } catch (error: any) {
      message.error(error.message);
    }
  };
  return (
    <div className="sign-in">
      <div className="sign-in__left">
        <div className="sign-in__left--container">
          <div className="title">
            <h3>Welcome !</h3>
            <h2>Sign in to</h2>
          </div>
          <Form
            layout="vertical"
            className="input"
            name="input"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off">
            <Form.Item
              label="Username"
              name="username"
              rules={[{ required: true, message: 'Please input your username!' }]}>
              <Input size="large" className="input__margin-top-bottom" placeholder="Input username" />
            </Form.Item>
            <Form.Item
              label="Password"
              name="password"
              rules={[{ required: true, message: 'Please input your password!' }]}>
              <Input.Password size="large" className="input__margin-top-bottom" placeholder="Input password" />
            </Form.Item>
            <Button size="large" className="sign-in-btn" htmlType="submit" type="primary" block>
              Sign In
            </Button>
          </Form>
          <div className="register">
            <h3>
              Don’t have an Account ? <span onClick={showRegistrationPopup}>Register</span>
            </h3>
            <Registration isShowed={registrationPopup} setVisible={setRegistrationPopup} />
          </div>
        </div>
      </div>
      <div className="sign-in__right">
        <img src={signInImage} alt="sign in image" />
      </div>
    </div>
  );
}
