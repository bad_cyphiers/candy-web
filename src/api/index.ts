import { IUserRegisterRequest, IUserRegisterResponse, IUserLoginRequest, IUserLoginResponse } from 'shared/types';
import apiBuilder from './apiBuilder';
import callApi from './apiHandler';

interface ApiWorker {
  getUserRegister: (params: IUserRegisterRequest) => Promise<IUserRegisterResponse>;
  getUserLogin: (params: IUserLoginRequest) => Promise<IUserLoginResponse>;
}

const api: ApiWorker = {
  getUserRegister: (params: IUserRegisterRequest) => callApi<IUserRegisterResponse>(apiBuilder.getUserRegister(params)),
  getUserLogin: (params: IUserLoginRequest) => callApi<IUserLoginResponse>(apiBuilder.getUserLogin(params)),
};

export default api;
