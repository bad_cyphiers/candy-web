import { AxiosRequestConfig } from 'axios';
import { IUserRegisterRequest, IUserLoginRequest } from 'shared/types';

export interface ApiBuilder {
  getUserRegister: (params: IUserRegisterRequest) => AxiosRequestConfig;
  getUserLogin: (params: IUserLoginRequest) => AxiosRequestConfig;
}

const apiBuilder: ApiBuilder = {
  getUserRegister: (params) => ({
    method: 'POST',
    data: params,
    url: `api/v1/register`,
  }),
  getUserLogin: (params) => ({
    method: 'POST',
    data: params,
    url: `api/v1/authenticate`,
  }),
};

export default apiBuilder;
