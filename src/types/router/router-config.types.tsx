export interface RouteProps {
  element: JSX.Element;
  path: string;
  children: RouteProps[] | null;
}
