import React, { lazy } from 'react';
import { RouteProps } from 'types/router/router-config.types';

const SignIn = lazy(() => import('../pages/signin'));
const AppointmentList = lazy(() => import('pages/appointment-list'));
const AppointmentDetails = lazy(() => import('pages/appointment-details'));
const CandidateList = lazy(() => import('../pages/candidate-list'));
const CandidateDetails = lazy(() => import('../pages/candidate-details'));
const UserProfile = lazy(() => import('../pages/user-profile'));
const ViewCV = lazy(() => import('../pages/view-cv'));

export const privateRoutes: RouteProps[] = [
  {
    element: <SignIn />,
    path: '/',
    children: null,
  },
];

export const publicRoutes: RouteProps[] = [
  {
    element: <AppointmentList />,
    path: 'appointment-list',
    children: null,
  },
  {
    element: <CandidateList />,
    path: 'candidate-list',
    children: null,
  },
  {
    element: <UserProfile />,
    path: 'user-profile/:user_id',
    children: null,
  },
  {
    element: <ViewCV />,
    path: 'view-cv/:candidate_id',
    children: null,
  },
  {
    element: <AppointmentDetails />,
    path: 'appointment-details',
    children: [
      {
        element: <AppointmentDetails />,
        path: ':appointment_id',
        children: null,
      },
    ],
  },
  {
    element: <CandidateDetails />,
    path: 'candidate-details',
    children: [
      {
        element: <CandidateDetails />,
        path: ':candidate_id',
        children: null,
      },
    ],
  },
];
