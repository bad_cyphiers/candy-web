import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

const AppRoute = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<AppRoute />} />
      </Routes>
      {/* <Suspense fallback={<LoadingFallBack />}>
        <Routes>
          {publicRoutes.map((route, index) => {
            return (
              <Route key={index} path={route.path} element={route.element}>
                {route.children &&
                  route.children.map((child) => {
                    return <Route key={`children/${index}`} path={child.path} element={child.element} />;
                  })}
              </Route>
            );
          })}
          {privateRoutes.map((route, index) => {
            return (
              <Route key={index} path={route.path} element={route.element}>
                {route.children &&
                  route.children.map((child) => {
                    return <Route key={`children/${index}`} path={child.path} element={child.element} />;
                  })}
              </Route>
            );
          })}
        </Routes>
      </Suspense> */}
    </BrowserRouter>
  );
};

export default AppRoute;
